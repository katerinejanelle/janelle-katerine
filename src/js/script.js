document.addEventListener("DOMContentLoaded", function (event) {

    var intervalId = 0;
    var scrollButton = document.querySelector('#retour');

    function flecheHaut() {

        if (window.pageYOffset === 0) {
            clearInterval(intervalId);
        }
        window.scroll(0, window.pageYOffset - 50);
    }

    function haut() {
        // Call the function scrollStep() every 16.66 millisecons
        intervalId = setInterval(flecheHaut, 16.66);
    }

// When the DOM is loaded, this click handler is added to our scroll button
    scrollButton.addEventListener('click', haut);


    //Fenêtre modale

    var boutonModale = document.querySelector(".fermer");
    var clickFenetre = document.querySelector(".login-container");
    var connexion = document.querySelector(".connexion");

    boutonModale.addEventListener("click", function () {


        document.querySelector(".login-container").style.display = "none";

    });

    /*clickFenetre.addEventListener("click", function () {


        document.querySelector(".login-container").style.display = "none";

    });*/

    connexion.addEventListener("click", function () {


        document.querySelector(".login-container").style.display = "flex";

    });



    //Article click

    var articleClick = document.querySelectorAll(".lienArticle");

    for (var i = 0; i < articleClick.length; i++) {

        articleClick[i].addEventListener("click", function () {
            location.replace("article.html");
        });
    }
});